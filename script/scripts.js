const themeToggleBtn = document.getElementById('themeToggle');
const body = document.body;
const savedTheme = localStorage.getItem('theme');
function toggleTheme() {
    if (body.classList.contains('dark')) {
        body.classList.remove('dark');
        localStorage.setItem('theme', 'default');
    } else {
        body.classList.add('dark');
        localStorage.setItem('theme', 'dark');
    }
}
themeToggleBtn.addEventListener('click', toggleTheme);
if (savedTheme === 'dark') {
    body.classList.add('dark');
}
